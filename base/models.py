from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Order(models.Model):
    
     date_from=models.DateTimeField()
     date_end=models.DateTimeField()  
     nb_vacation=models.IntegerField(default=0.0)
     reason = models.CharField(max_length=2500)
     user=models.ForeignKey(User,null=True, on_delete = models.CASCADE)
     
     def __str__(self):
         return self.user.username
    