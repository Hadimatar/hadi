from django.forms import ModelForm, fields
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms

from .models import Order

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username','email','password1','password2']

class VacationRegistration(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['date_from','date_end','reason']
        widgets = {
            'date_from': forms.DateInput(
                attrs={'class': 'form-control', 
                       'placeholder': 'Select a date',
                       'type': 'date'  # <--- IF I REMOVE THIS LINE, THE INITIAL VALUE IS DISPLAYED
                    }),
            'date_end': forms.DateInput(
                attrs={'class': 'form-control', 
                       'placeholder': 'Select a date',
                       'type': 'date'  # <--- IF I REMOVE THIS LINE, THE INITIAL VALUE IS DISPLAYED
                    }),
            'reason': forms.Textarea(
                attrs={ 'class':'form-control',
                        'rows': '3'
                    }),
        }