from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.db.models.aggregates import Sum
from django.db.models.expressions import F
from django.http.response import HttpResponseRedirect
from django.shortcuts import render,redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from .forms import CreateUserForm, VacationRegistration
from .models import Order
from django.contrib.auth import authenticate, login, logout
from datetime import datetime as dt
from django.contrib import messages

# Create your views here.


def indexView(request):
    return render(request,'accounts/index.html')

#this function will add new vacation and show all vacations
@login_required
def home(request):
    try:
        orders = Order.objects.filter(user=request.user).all()
        nb_vacations=orders.aggregate(Sum('nb_vacation'))
        for i in nb_vacations:
            if nb_vacations:
                nb_vacations=0
            else:    
                nb_vacations=nb_vacations[i]
    except Order.DoesNotExist:  
        orders = None
        nb_vacations=0
    if request.method == 'POST':

        fm = VacationRegistration(request.POST)
        if fm.is_valid():
    
            datef = fm.cleaned_data['date_from']
            datee = fm.cleaned_data['date_end']
            re = fm.cleaned_data['reason']
            n=datee-datef
            nbv = abs((datee-datef).days)
        
            if datee > datef:
                if datef.timestamp() < dt.now().timestamp():   
                    messages.info(request,'You choose an incorrect From Date')    
                    return render(request,'accounts/dashboard.html')    
                else:
                    if nb_vacations > 16:
                        messages.info(request,"You Can't Take over 16 vacations")    
                        return render(request,'accounts/dashboard.html')
                    else:         
                        if nbv > 16:
                            messages.info(request,'Your are take all vacations')    
                            return render(request,'accounts/dashboard.html') 
                        else:       
                            reg=Order(date_from=datef, date_end=datee, nb_vacation=nbv,user=request.user,reason=re)
                            reg.save()
                            fm = VacationRegistration()
            else:
                messages.info(request,'Dates Incorrect')    
                return render(request,'accounts/dashboard.html')    
   
    else : 
        fm = VacationRegistration()
    
    try:
        orders = Order.objects.filter(user=request.user).all()
        nb_vacations=orders.aggregate(Sum('nb_vacation'))
        for i in nb_vacations:
            nb_vacations=nb_vacations[i]
    except Order.DoesNotExist:  
        orders = None

    context = {'form':fm , 'emp':orders, 'nb_v':nb_vacations}

    return render(request, 'accounts/dashboard.html', context)

def loginView(request):
    if request.method == "POST":

        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
   
        if user is not None:
            login(request, user)
            return redirect('home') 
        else:
            messages.info(request,'Username or password incorrect')    
            return render(request,'accounts/registration/login.html')
    context = {}
    return render(request,'accounts/registration/login.html')

def registerView(request):
    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login_url') 
    else:
        form = CreateUserForm()
    return render(request,'accounts/registration/register.html',{'form':form})


def logoutView(request):
    return redirect('login')


#this function will delete
@login_required
def delete_data(request,id):
    if request.method == 'POST':
        pi = Order.objects.get(pk=id)
        if pi.date_from.timestamp() < dt.now().timestamp():    
            return HttpResponseRedirect('../../dashboard')    
        else:
            pi.delete()
        return  HttpResponseRedirect('../../dashboard')   
    
#this function will update
@login_required
def update_data(request,id):

    if request.method == 'POST':
        
        pi = Order.objects.get(pk=id)
        fm=VacationRegistration(request.POST,instance=pi)
        if fm.is_valid():
            fm.save()
    else:
       pi = Order.objects.get(pk=id)
       fm=VacationRegistration(instance=pi)         
    return render(request,'accounts/updateorder.html', {'form':fm})     